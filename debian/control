Source: avogadro
Maintainer: Debichem Team <debichem-devel@lists.alioth.debian.org>
Uploaders: Michael Banck <mbanck@debian.org>
Section: science
Priority: optional
Build-Depends: cmake,
               debhelper-compat (= 13),
               doxygen,
               libavogadro-dev (>= 1.96~),
               libeigen3-dev (>> 3.3),
               libgl2ps-dev,
               libglew-dev,
               libhdf5-dev,
               libopenbabel-dev,
               libsymspg-dev,
               molequeue, libmolequeue-dev,
               pkg-config,
               qtbase5-dev,
               zlib1g-dev
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/debichem-team/avogadro
Vcs-Git: https://salsa.debian.org/debichem-team/avogadro.git
Homepage: http://avogadro.cc/

Package: avogadro
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Recommends: avogadro-utils,
 molequeue
Description: Molecular Graphics and Modelling System
 Avogadro is a molecular graphics and modelling system targeted at molecules
 and biomolecules.  It can visualize properties like molecular orbitals or
 electrostatic potentials and features an intuitive molecular builder.
 .
 Features include:
  * Molecular modeller with automatic force-field based geometry optimization
  * Molecular Mechanics including constraints and conformer searches
  * Visualization of molecular orbitals and general isosurfaces
  * Visualization of vibrations and plotting of vibrational spectra
  * Support for crystallographic unit cells
  * Input generation for the Gaussian, GAMESS and MOLPRO quantum chemistry
    packages
  * Flexible plugin architecture and Python scripting
 .
 File formats Avogadro can read include PDB, XYZ, CML, CIF, Molden, as well as
 Gaussian, GAMESS and MOLPRO output.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: avogadroapp
Source: https://github.com/OpenChemistry/avogadroapp

Files: *
Copyright: 2011-2020, Kitware, Inc.
License: BSD-3-clause

Files: avogadro/icons/avogadro2.appdata.xml
 flatpak/org.openchemistry.Avogadro2.metainfo.xml
Copyright: 2020, 2022 Open Chemistry team
License: CC0-1.0
 On Debian systems the full text of the Creative Commons
 CC0 1.0 Universal licence can be found at
 /usr/share/common-licenses/CC0-1.0

Files: debian/*
Copyright: 2007 Jordan Mantha <mantha@ubuntu.com>.
 2008-2019 debichem team <debichem-devel@lists.alioth.debian.org>
License: GPL

Files: debian/avogadro-i18n/*
Copyright: 2018, Open Chemistry
License: BSD-3-clause
Comments:
 git subtree fetched from https://github.com/OpenChemistry/avogadro-i18n

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
 .
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
 .
    * Neither the name of the Avogadro project nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KITWARE, INC.
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL
 GPL-2+: see /usr/share/common-licenses/GPL
 or /usr/share/common-licenses/GPL-2
